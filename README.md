### Lepus监控系统:
1、OS性能、MySql、Oracle、MongoDB、Redis <br>
2、WEB进程数、进程内存监控、TCP连接数监控、PV、UV、IP值统计、web错误日志及时提醒
### 需要开发环境：
centos7+php5.6+mysql5.6+apache+python2.7



### 示例配置：
监控WEB服务器IP:192.168.3.59 <br>
监控DB服务器IP:192.168.3.60 <br>
脚本存放在目录:/data/www/ <br>
客户端IP：192.168.3.15 <br>
#### 一、后台python配置
yum install -y  python-devel MySQL-python python-paramiko mysql python2-pip <br>
pip install redis <br>
pip install redis-py-cluster <br>
pip install cx_oracle pymongo pymssql <br>

配置数据库IP、用户、密码路径:
cd /data/www/lepus/lepus/etc <br>
1、cd /data/www/lepus/lepus <br>

修改目录路径：basedir="/data/www/lepus/lepus"  <br>
2、启动 <br>
./lepus start <br>

#### 二、前端WEB配置 <br>
cd /data/www/lepus/web/application/config <br>
配置config.php文件下 $config['base_url']     = 'http://xxxx/; <br>
配置database.php下连接db信息 <br>

备注：
登陆用户admin 密码123abc <br>
修改密码sql: <br>
UPDATE admin_user SET `password`=MD5('123ab') WHERE username='admin'; <br>

升级用户更新sql,只执行sql/update_web.sql <br>
全新用户更新sql顺序:sql/lepus_table.sql, lepus_data.sql , update_web.sql <br>

#### 三、获得web服务器客户SSH秘钥 <br>
由于获取web服务器访问日志、pv、uv信息，所以需要ssh密钥登陆 <br>
在客户端192.168.3.15 <br>
用ssh-keygen 的-f和-P参数，生成密钥不需要交互 <br>
ssh-keygen -t rsa -f ~/.ssh/id_rsa -P '' <br>
cp ~/.ssh/id_rsa.pub   ~/.ssh/authorized_keys <br>
复制id_rsa内容界面：配置中心->操作系统->新增或修改对应主机下RSA框里 <br>

#### 四、配置web日志、PV、IP、UV
1、安装日志分隔工具：yum install -y cronolog <br>

2、apache日志格式：<br>

添加UV值
vim httpd.conf
启用mod_usertrack模块 <br>
```LoadModule usertrack_module libexec/mod_usertrack.so 
<IfModule usertrack_module> 
CookieExpires “1 weeks”
CookieStyle cookie
CookieName your_cookie_name
CookieTracking on
</IfModule>
```

访问日志中记录真实的客户 IP 地址 <br>
SetEnvIf X-Forwarded-For "^.*\..*\..*\..*" forwarded <br>
在日志LogFormat最后增加%{cookie}n字段 <br>
LogFormat "%{X-Forwarded-For}i %{cookie}n %{%Y-%m-%d %H:%m:%S}t %r %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" common <br>
X-Forwarded-For 客户端IP <br>
cookie 是指UV值  <br>
%Y-%m-%d %H:%m:%S 访问日期时间 <br>
%r 请求的行对应格式为”%m %U%q %H”，即”请求方法/访问路径/协议” <br>
注意：日志格式顺序与参数不要变动，否则你需要更改/data/www/lepus/lepus/check_web_log.sh 脚本文件；
当然还得修改check_web_log.sh脚本排除web访问的非动态URL，如css、image、upfile目录
<br>
配置web访问日志格式：<br>

CustomLog "|/usr/sbin/cronolog /data/server/httpd/logs/xx.com-access_%Y%m%d.log" common env=forwarded <br>
xx.com-access_%Y%m%d.log按天分隔%Y%m%d,对应界面:配置中心->操作系统->新增或修改对应“访问日志规则” <br>

3、nginx 日志格式配置 <br>
nginx 添加uv <br>
```
http {
     log_format  main  '$http_x_forwarded_for $uid $http_x_cookie $time_iso8601 $request $remote_addr - $remote_user [$time_local] "$request" $st
atus $body_bytes_sent "$http_referer" "$http_user_agent" "$http_x_forwarded_for"';
}

server {
        set $uid "-";
        if ( $http_cookie ~* "uid=(\S+)(;.*|$)")
        {
                set $uid $1;
        }
}
```
注意：日志格式顺序与参数不要变动，否则你需要更改/data/www/lepus/lepus/check_nginx_web_log.sh  脚本文件；<br>

如需要帮助请加 wechat:271416962
